'use strict'

const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Started !' }
})

//User Auth
Route.post('/user/add','UserController.register')
Route.get('/user/:id', 'UserController.index')

//OTP Auth
Route.post('/otp/add', 'OtpController.new')
Route.post('/otp/revoke', 'OtpController.revoke')
Route.post('/otp/verify', 'OtpController.verify')

//Kavenegar
Route.get('/sms/:phone', 'SmController.send')

//Confirm && JWT Auth 
Route.get('/otp/:otp', 'UserController.login')