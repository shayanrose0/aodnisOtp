'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use('Hash')

class User extends Model {
  
  tokens () {
    return this.hasMany('App/Models/Token')
  }
  otps () {
    return this.hasMany('App/Models/Otp')
  }
}

module.exports = User
