'use strict'
var Kavenegar = require('kavenegar');
const Otp = use('App/Models/Otp')
const User = use('App/Models/User')
const Env = use('Env')

class SmController {

    async send ({ response,auth, params }) {

        var code = Math.floor(Math.random() * 10000) + 12345;
        var Message = `code:${code}`
        let Customer = params.phone
        
        var api = Kavenegar.KavenegarApi({
           apikey: Env.get('kavenegarApi', '')
         });
   
         api.Send({
           message: Message,
           sender: Env.get('kavenegarSender', ''),
           receptor: Customer
         });   

         const user = await User.findOrCreate(
          { phone: Customer },
          { phone: Customer, name: 'guest' }
        )
        //const token = await auth.generate(user)


          const addotp = new Otp()
          addotp.user_id = user.id
          addotp.otp = code
          addotp.is_revoked = false
          addotp.save()

          response.send(Message)
     }
}

module.exports = SmController
