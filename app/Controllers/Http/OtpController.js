'use strict'
const Otp = use('App/Models/Otp')

class OtpController {

    async new({request, response}) {

        const {user_id,otp,is_revoked} = request.all()
        
        let addotp = await Otp.create({user_id,otp,is_revoked})

        return response.status(201).json(addotp)
      }

    async revoke({request, response}) {

        const {otp,is_revoked} = request.all()

        const revokotp = await Otp.findBy('otp', otp)

        revokotp.is_revoked = is_revoked

        await revokotp.save()

        return response.json(revokotp)
      }

      async verify({request, response}) {

        const {otp} = request.all()

        const revokotp = await Otp.findBy('otp', otp)

        if(revokotp.is_revoked == 0){
            return response.json("verify")
        }
        else {
            return response.json("error")
        }
      }
}

module.exports = OtpController
