'use strict'
const User = use('App/Models/User')
const Otp = use('App/Models/Otp')

class UserController {

    async index({params,response}) {
        const getUsers = await User.query()
                        .where('id', params.id)
                        .with('otps')
                        .firstOrFail()
        return response.json(getUsers)
      }


    async register({request, auth, response}) {

        const {name,phone} = request.all()
        let user = await User.create({name,phone})

        //generate token for user;
        let token = await auth.generate(user)

        Object.assign(user, token)

        return response.json(user)
      }

      async login({params,auth,response}){
        try {
            const getOtp = await Otp.findBy('otp',params.otp)
            const UserId = getOtp.user_id
            getOtp.is_revoked = 1
            await getOtp.save()
            
            const getUser = await User.find(UserId)
            const token = await auth.generate(getUser)

            return response.json({
              status: 'success',
              data: token
          })
        }

        catch (error) {
          response.status(400).json({
              status: 'error',
              message: 'Invalid'
          })
      }
        //return response.json(token)
      }

}

module.exports = UserController
